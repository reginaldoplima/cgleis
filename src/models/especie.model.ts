export enum Especie {

  ConstEstadudal = "Constituição Estadual",
  ConstFederal = "Constituição Federal",
  CodVigilanciaSanitaria = "Código de Vigilância Satitária",
  CodTributarioEstadual = "Código Tributario Estadual",
  Decretos = "Decretos",
  EstatutoIdoso = "Estatuto do Idoso",
  EstatutoServidor = "Estatuto do Servidor",
  LeiDiretrizesOrcamentaria = "Lei de Diretrizes Orcamentaria",
  LeiOrganica = "Lei Orgânica",
  LeiOrcamentaria = "Lei Orcamentaria",
  Leis = "Leis",
  LeisConsumeristas = "Leis Consumeristas",
  Semanarios  = "Semanários"

}
