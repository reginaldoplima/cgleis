import { Especie } from ".";

export class Lei {

  constructor(id?:string, ementa?: string, ano?:string, url?:string, especie?:string, 
    numero?:string  ) {
      this.id = id;
      this.ano = ano;
      this.numero = numero;
      this.ementa = ementa;
      this.especie = especie;
      this.urlPdf = url;
  }
  public id:string;
  public numero: string;
  public ementa:string;
  public localFile: string;
  public ano:string;
  public urlPdf:string;
  public especie: string;
  public favorita: boolean;
}
