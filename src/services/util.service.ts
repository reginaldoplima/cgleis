import { Injectable } from '@angular/core';
import { HTTPResponse } from '@ionic-native/http';

@Injectable()
export class Util {

  public static checkNoNetwork(response: HTTPResponse) {
      if(response.status === 0 ){
        return true;
      }
      return false;
  }
}
