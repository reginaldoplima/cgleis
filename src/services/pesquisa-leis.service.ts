import {Especie} from '../models';
import { HTTP, HTTPResponse } from '@ionic-native/http';
import { Injectable } from '@angular/core';
import { Pesquisa, Lei } from '../models';
import { Observable  } from 'rxjs/Observable';
import Rx from 'rxjs/Rx';


@Injectable()
export class PesquisaLeisService {

  public static baseUrlApi: string = "http://191.253.16.179:8080/cgleis/api/leis";
  private headers: any = { Accept: "application/json" };
  constructor(private http: HTTP ) {
  }

  pesquisar(pesquisa: Pesquisa) : Observable <Lei[]> {

    const leis: Lei[] = [
           new Lei("1" ,
              "csdadasdasdasdasdasda adsadasdas dasdadasdas adasdas asdas",
              "2018", "http://cartasdecristobrasil.com.br/wp-content/uploads/2017/03/carta7.pdf", Especie.Leis ),
           new Lei("1" ,
           "csdadasdasdasdasdasda adsadasdas dasdadasdas adasdas asdas", "2018", "http://cartasdecristobrasil.com.br/wp-content/uploads/2017/03/carta7.pdf",
           Especie.LeiDiretrizesOrcamentaria ),
      ];
      return Rx.Observable.of(leis);
  }
  pesquisarPromise (pesquisa: Pesquisa) : Promise<HTTPResponse> {
    // console.log(pesquisa);  
    return this.http.get( PesquisaLeisService.baseUrlApi , pesquisa , this.headers );
  }
  setBaseUrl(url:string) {
    PesquisaLeisService.baseUrlApi = url;
  }
}
