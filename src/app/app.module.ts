import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';



import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CodePush } from '@ionic-native/code-push';
import { FileTransfer } from '@ionic-native/file-transfer';
import { HTTP } from '@ionic-native/http';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { Dialogs } from '@ionic-native/dialogs';


import { PesquisaLeisService } from '../services/pesquisa-leis.service';
import { ListaLeisPageModule } from '../pages/lista-leis/lista-leis.module';
import { FavoritosPageModule } from '../pages/favoritos/favoritos.module';
import { FavoritosPage } from '../pages/favoritos/favoritos';
import { CGLeis } from './app.component';
import { PesquisaPage } from '../pages/pesquisa/pesquisa';


@NgModule({
  declarations: [
    CGLeis,
    PesquisaPage,
  ],
  imports: [
    BrowserModule,
    ListaLeisPageModule,
    FavoritosPageModule,
    IonicModule.forRoot(CGLeis),
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    CGLeis,
    PesquisaPage,
    FavoritosPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    CodePush,
    FileTransfer,
    File,
    FileOpener,
    HTTP,
    Dialogs,

    PesquisaLeisService,
    {provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule {}
