import { PesquisaPage } from './../pages/pesquisa/pesquisa';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { CodePush, SyncStatus } from '@ionic-native/code-push';
import { SplashScreen } from '@ionic-native/splash-screen';
import { FavoritosPage } from '../pages/favoritos/favoritos';

@Component({
  templateUrl: 'app.html'
})
export class CGLeis {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = PesquisaPage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform,
              public statusBar: StatusBar,
              private codePush: CodePush,
              private toast: ToastController,
              public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Pesquisa', component: PesquisaPage },
      { title: 'Favoritas', component: FavoritosPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      // note - mostly error & completed methods of observable will not fire
      // as syncStatus will contain the current state of the update
      this.codePush.sync().subscribe((syncStatus) => this.onCodePushSync(syncStatus));
      const downloadProgress = (progress) => {
        console.log(`Downloaded ${progress.receivedBytes} of ${progress.totalBytes}`);
      }
      this.codePush.sync({}, downloadProgress).subscribe((syncStatus) =>
        this.onCodePushSync(syncStatus));
    });
  }

  onCodePushSync(status:SyncStatus) {
    // console.log(status);
    let msg = "Code push ";
    switch (status) {
      case SyncStatus.IN_PROGRESS:
        this.presentToast("Atualizações em progresso...");
        break;

      case SyncStatus.CHECKING_FOR_UPDATE:
        this.presentToast("Checando Atualizações...");
        break;

      case SyncStatus.DOWNLOADING_PACKAGE:
         this.presentToast("Download de Atualizações...");
        break;

      case SyncStatus.INSTALLING_UPDATE:
        this.presentToast("Instalando Atualizações...");
        break;

      case SyncStatus.UPDATE_INSTALLED:
        this.presentToast("Atualizações Instaladas! Reinicie o aplicativo.");
        break;

      case SyncStatus.ERROR:
        console.error(msg+"algo deu errado!!!")
        break;

      default:
        console.log(msg+"deu ruim")
        break;
    }
  }

  presentToast(msg:string) {
    let toast = this.toast.create({
      message: msg,
      duration: 3000,
      position: 'middle'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
