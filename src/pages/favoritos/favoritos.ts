import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Lei } from '../../models';

import { ListaLeisPage } from '../lista-leis/lista-leis';

@IonicPage()
@Component({
  selector: 'page-favoritos',
  templateUrl: 'favoritos.html',
})
export class FavoritosPage {

  constructor(public navCtrl: NavController,
              private storage: Storage,
              public navParams: NavParams) {
  }
  leis: Lei [] = [];
  ionViewWillLoad() {
    // console.log("vai");
    this.onViewWillLoad();
  }
  goToLeis() {
    // console.log(this.leis);
    const leis = {
      leis: this.leis
    }
    this.navCtrl.push(ListaLeisPage , leis);
  }
  pushLei(lei:any) {
    const leiObj = lei as Lei;
    this.leis.push(leiObj);
  }
  
  onViewWillLoad(){
    this.storage.forEach( (v,k,n) => this.pushLei(v) ).then(
      () => console.log("")
    );
  }
  
  limparFavoritas() {
    this.storage.clear().then(
      ()=> this.leis = []
    );
  }

}
