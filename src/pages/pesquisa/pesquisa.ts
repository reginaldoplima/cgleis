
import { Component } from '@angular/core';
import { Dialogs } from '@ionic-native/dialogs';
import { NavController, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { ListaLeisPage } from '../lista-leis/lista-leis';
import { Pesquisa, Especie, Lei } from '../../models';
import { PesquisaLeisService } from '../../services/pesquisa-leis.service';

@Component({
  selector: 'page-pesquisa',
  templateUrl: 'pesquisa.html'
})
export class PesquisaPage implements OnInit {
  anos: string[] = [];
  pesquisa: Pesquisa = new Pesquisa();
  ids: string [] = [];
  ngOnInit(): void {
    const anoInicial = 1900;
    const esseAno = new Date().getFullYear();
    const rangeDeAnos = esseAno - anoInicial;
    this.anos = Array.apply(null, {length: rangeDeAnos}).map(Function.call,
      i=>{
        return  (i+1) + anoInicial;
    }).reverse();
    this.anos.push("");
    this.pesquisa.ano = "";
    this.pesquisa.buscaTextual = "";
    this.pesquisa.especie = "Leis";
    this.pesquisa.numero = "";
    this.pesquisa.ementa = "";
    this.storage.keys().then(
      keys => this.ids = keys
    );
  }

  loading = this.loadingCtrl.create({
    content: 'Carrengando Leis... Aguarde!'
  });
  constructor(public navCtrl: NavController,
              private dialogs: Dialogs,
              private storage: Storage,
              public loadingCtrl: LoadingController,
              public pesquisaService: PesquisaLeisService) {
  }
  alertPesquisaGenerica(pesquisa: Pesquisa) {
    
    if(pesquisa.buscaTextual === "" && pesquisa.ano === "" && pesquisa.numero === ""  && pesquisa.ementa === "" ) {
      this.dialogs.confirm("A pesquisa retornara um grande número de leis. Deseja continuar?", "Pesquisa muito genêrica").then(
        (confirm) => {
          if(confirm != 2) {
            this.pesquisar(pesquisa);
          }
        }
      );
    } else {
      this.pesquisar(pesquisa);
    }

  }
  public pesquisar(pesquisa: Pesquisa) {
    // console.log("search");
    let pesquisaObj: Pesquisa = new Pesquisa();
    Object.assign(pesquisaObj , pesquisa);
    pesquisaObj.especie = pesquisa.especie 
       ? encodeURI(pesquisa.especie)   : pesquisa.especie;
    
    if(pesquisa.buscaTextual) {
      pesquisaObj.buscaTextual =  encodeURI(this.pesquisa.buscaTextual);
    }
    console.log(pesquisaObj);
    this.presentLoadingDefault();
    this.pesquisaService.pesquisarPromise(pesquisaObj).then((response) => {
      // console.log(response.status);
      const objLeis = JSON.parse(response.data);
      const leis: Lei[] = [];
      objLeis.forEach(objLei => {
        let isFavorita:boolean = false;
        const key = "lei_" + objLei.Id;
        isFavorita = this.ids.indexOf(key) > -1;
        const lei = new Lei(objLei.Id,objLei.Ementa, 
            objLei.Ano, objLei.UrlPdf, Especie.Leis, objLei.Numero );
        lei.favorita = isFavorita;  
        leis.push(lei);
      });
      this.onLoadSucess(leis);
      this.loading.dismiss();
    }, (error) => {
      console.log(error);
      this.loading.dismiss();
      this.dialogs.alert("Servidor fora do ar ou Você não tem conexão de internet.", 
          "Erro de conexão!!! :(").then(
        () => {
          console.log(error);
        }
      ).catch( (e) => { this.loading.dismiss();  console.log(e); }  );
    }).catch( (e) => { this.loading.dismiss();  console.log(e); } );
  }
  
  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      content: 'Carrengando Leis... Aguarde!'
    });
    this.loading.present();
  }
  limpar() {
    console.log("Limpando...")
    this.pesquisa.ano = "";
    this.pesquisa.buscaTextual = "";
    this.pesquisa.ementa = "";
    this.pesquisa.especie = "Leis";
    this.pesquisa.numero = "";
  }

  public onLoadSucess(result) {
    const leis = {
      leis: result
    }
    this.navCtrl.push(ListaLeisPage , leis);
  }
  

}
