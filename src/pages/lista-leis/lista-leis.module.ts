import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListaLeisPage } from './lista-leis';
import { PesquisaLeisService } from '../../services/pesquisa-leis.service';

@NgModule({
  declarations: [
    ListaLeisPage,
  ],
  imports: [
    IonicPageModule.forChild(ListaLeisPage),
  ],
  providers: [ PesquisaLeisService ],
})
export class ListaLeisPageModule {}
