import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { FileTransfer,  FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { Storage } from '@ionic/storage';
import { Lei } from '../../models/leis.model';


@IonicPage()
@Component({
  selector: 'page-lista-leis',
  templateUrl: 'lista-leis.html',
})
export class ListaLeisPage {

  leis: Lei[] = [];

  constructor(public navParams: NavParams,
              private fileOpener: FileOpener,
              public loadingCtrl: LoadingController,
              private storage: Storage,
              private transfer: FileTransfer, private file: File) {
  }

  ionViewDidLoad() {
    this.leis = this.navParams.get('leis');
    // console.log('ionViewDidLoad ListaLeisPage');
  }

  onGetLocalSuccess(result_lei:Lei, lei:Lei ,key:string) {
    console.log(result_lei);
    if( result_lei ) {
      this.storage.remove(key).then( (result) => lei.favorita = false )
    }
    else {
      lei.favorita = true
      this.storage.set(key, lei).then(
        (result) => lei.favorita = true ,
        (e) => console.error(e)
      ).catch( (e) => console.error(e) );
    }
  }

  favoriteLei(lei:Lei) {
    const key:string = "lei_" + lei.id;
    this.storage.get(key).then( (result_lei)=> this.onGetLocalSuccess(result_lei, lei , key));
  }

  abrirPdf(nativeURL:any) {
    this.fileOpener.open( nativeURL, 'application/pdf')
    .then(() => console.log('Arquivo aberto') )
    .catch(e => console.log('Error openening file', e));
  }
  
  onProgress(progress:ProgressEvent, loadingView: Loading) {
    let total = Math.trunc((progress.loaded / progress.total)*100);
    console.log(total);
    if(total === 100) {
      loadingView.setContent("Aberto!");
    } else {
      loadingView.setContent(total+ "%");
    }
    
  }

  downloadPdf(source:string, target:string) {
    let loading = this.loadingCtrl.create({
      content: 'Carregando Arquivo...'
    });
    loading.present();
    const fileTransfer: FileTransferObject = this.transfer.create();
    fileTransfer.onProgress((progress) => this.onProgress(progress, loading) );
    fileTransfer.download(source , target).then( (entry)=> {
      console.log(entry);
      loading.dismiss();
      this.abrirPdf(entry.nativeURL);
    }, (error)=> {
      loading.dismiss();
      console.error(error)
    });
  }

  abrir(lei:Lei) {
    const url = lei.urlPdf;
    var filename = url.substring(url.lastIndexOf('/')+1);
    let filepath = this.file.dataDirectory.substring(0, this.file.dataDirectory.lastIndexOf('/') + 1);
    const target = this.file.dataDirectory + filename;
    let fileExist = false;
    console.log("diretorio",this.file.dataDirectory);
    console.log("filepath",filepath);
    console.log("nome",filename);
    this.file.checkFile(filepath,filename).then(
      (result) => {
        console.log(result);
        fileExist = result;
        if(!result) {
          console.log("Arquivo nao existe");
          this.downloadPdf(url,target);
        }else {
          console.log("Existe ja o arquivo");
          this.abrirPdf(target);
        }
      },
      (error) => {
        console.log("Arquivo nao existe");
        this.downloadPdf(url,target);
      }
    ).catch((error) => console.log(error));
  }

}
